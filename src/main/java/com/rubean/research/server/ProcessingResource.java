package com.rubean.research.server;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.file.Files;
import java.time.Duration;
import java.time.Instant;
import java.util.Timer;
import java.util.UUID;

@Path("api")
public class ProcessingResource {

    private static final String SOME_STATIC_STRING = UUID.randomUUID().toString();

    @Path("xslx/{rows}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response generateLargeFile(@PathParam("rows") int rows) {
        Instant startTime = Instant.now();

        Response response = getResponse(rows);

        System.out.println("Took " + Duration.between(startTime, Instant.now()).toMillis() + "ms");

        return response;
    }

    private static Response getResponse(int rows) {
        try (SXSSFWorkbook wb = new SXSSFWorkbook(10)) {
            Sheet sheet = wb.createSheet("MyNiceSheet");

            System.out.println("Writing rows");
            for(int rownum = 0; rownum < rows; rownum++){
                Row row = sheet.createRow(rownum);
                for(int cellnum = 0; cellnum < 30; cellnum++){
                    Cell cell = row.createCell(cellnum);
                    String address = new CellReference(cell).formatAsString();
                    cell.setCellValue(address + SOME_STATIC_STRING);
                }
            }

            System.out.println("Writing file");

            String fileName = UUID.randomUUID().toString() + ".xlsx";
            File fileNameFile = new File(fileName);


            FileOutputStream fileOutputStream = new FileOutputStream(fileNameFile);
            wb.write(fileOutputStream);

            fileOutputStream.close();

            System.out.println("Reading file for response");

            FileInputStream fileInputStream = new FileInputStream(fileNameFile);

            return Response.ok(fileInputStream, "application/octet-stream")
                    .header("Content-Length", Files.size(fileNameFile.toPath()))
                    .header("Content-Disposition", "attachment;filename=" + fileName)
                    .build();
        } catch (IOException e) {
            e.printStackTrace();

            return Response.serverError().entity(e).build();
        }
    }
}
